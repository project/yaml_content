<?php

namespace Drupal\yaml_content\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * A class for extending plugin api.
 *
 * @see plugin_api
 *
 * @Annotation
 */
class YamlContentProcess extends Plugin {

}
